// =============================================================================
// Copyright 2011-2020 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef WHAM_HPP
#define WHAM_HPP

namespace wham
{
void try_sprain_player();

void run();

}  // namespace wham

#endif  // WHAM_HPP
