// =============================================================================
// Copyright 2011-2020 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef DISARM_HPP
#define DISARM_HPP

namespace disarm
{
void player_disarm();

}  // namespace disarm

#endif
